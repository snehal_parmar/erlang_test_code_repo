-module(tut4).
-export([length_list/1]).

length_list([]) ->
    0;

length_list([F|R]) ->
    1 + length_list(R).