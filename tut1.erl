-module(tut1).
-export([fac/1]).

fac(1) ->
    1;
fac(X) ->
    X*fac(X-1).
